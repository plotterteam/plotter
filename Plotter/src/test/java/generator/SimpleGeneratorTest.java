package test.java.generator;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.Map;

import main.java.HelloPlotter;
import main.java.generator.ISimpleGenerator;
import main.java.generator.SimpleGenerator;

import org.junit.Before;
import org.junit.Test;



public class SimpleGeneratorTest {
	private ISimpleGenerator generator;
	private int size = 10;
	@Before
    public void setUp() throws Exception {
		generator = new SimpleGenerator(size);
    }
	
    @Test
    public void testGenerateMapSchouldReturnMapWithSpecifiedSize() {
        //given
    	
        //when
    	Map<String, Integer> map = generator.getActualWordsMap();
        //then
        assertEquals(map.size(), size);                                                                
    }
    
    @Test
    public void testGenerateMapSchouldReturnMapWithWordsAndNumbers() {
        //given
    	
        //when
    	Map<String, Integer> map = generator.getActualWordsMap();
        //then

        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            System.out.println(pairs.getKey() + " -- " + pairs.getValue());
        }
                                                                         
    }
}
