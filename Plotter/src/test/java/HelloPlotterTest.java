package test.java;

import static org.junit.Assert.*;
import main.java.HelloPlotter;

import org.junit.Before;
import org.junit.Test;

public class HelloPlotterTest {
	private HelloPlotter plotter;
	

    @Test
    public void testSayHello() {
        //given
    	plotter = new HelloPlotter();
        //when
    	String answer = plotter.sayHello();
        //then
        assertEquals(answer, "Plotter says hello!");
                                                                         
    }

}
