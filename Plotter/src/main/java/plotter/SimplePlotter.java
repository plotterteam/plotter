package main.java.plotter;


import java.awt.BorderLayout;
import java.util.List;
import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;



import javax.swing.JPanel;


public class SimplePlotter extends JPanel {
	public static final long serialVersionUID = 1;
	private DefaultCategoryDataset dataset;
	private JFreeChart chart;
	private ChartPanel panel;
	
	public SimplePlotter(){
		super(new BorderLayout());
		dataset = new DefaultCategoryDataset();
		chart = ChartFactory.createBarChart3D("Znalezione frazy",
				null, null, dataset, PlotOrientation.VERTICAL,
				false, true, false);
	 
		panel = new ChartPanel(chart);
		add(panel);
	}
	
	public void update(String phrase, Integer value){
		dataset.setValue(value, "found", phrase);
	}
	
	public void update(Map<String, Integer> results){
		for(Map.Entry<String, Integer> entry : results.entrySet()){
			dataset.setValue(entry.getValue(), "found", entry.getKey());
		}
	}
	
	/*public void smoothUpdate(String phrase, Integer value, int speed){
		Integer old = (Integer)dataset.getValue("found", phrase);
		while(value > old){
			old += Math.min(speed, value - old);
			update(phrase, old);
			try{
				Thread.sleep(200);
			}catch(InterruptedException e){
				
			}
		}
		while(value < old){
			old -= Math.min(speed, old - value);
			update(phrase, old);
			try{
				Thread.sleep(200);
			}catch(InterruptedException e){
				
			}
		}
	}
	
	public void smoothUpdate(Map<String, Integer> results, int speed){
		boolean changed = true;
		while(changed){
			changed = false;
			for(Map.Entry<String, Integer> entry : results.entrySet()){
				String phrase = entry.getKey();
				Integer value = entry.getValue();
				Integer old = (Integer)dataset.getValue("found", phrase);
				
				if(value > old){
					changed = true;
					old += Math.min(speed, value - old);
					update(phrase, old);
					try{
						Thread.sleep(200);
					}catch(InterruptedException e){
						
					}
				}
				else if(value < old){
					changed = true;
					old -= Math.min(speed, old - value);
					update(phrase, old);
					try{
						Thread.sleep(200);
					}catch(InterruptedException e){
						
					}
				}
			}
		}
	}*/
	
}
