package main.java.generator;



import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.annotation.Generated;

public class SimpleGenerator implements ISimpleGenerator{
	private HashMap<String, Integer> wordsMap = new HashMap<String, Integer>();
	private int numberOfWords; // number of words in the Map
	private final int growth = 6; //how fast the numbers are growing
	private final int stringSize = 6; //how big strings will be
	private Random random = new Random();
	
    public SimpleGenerator(int numberOfWords) {
		this.numberOfWords = numberOfWords;
	}
	@Override
	public Map<String, Integer> getActualWordsMap() {
		if(wordsMap.isEmpty()){
			generateWords();
			return Collections.unmodifiableMap(wordsMap); // returns read-only Map
		}
		updateWordsMap();
		return Collections.unmodifiableMap(wordsMap);
	}
	//puts words and Integers into Map
	private void generateWords(){
		for(int i=0; i<this.numberOfWords; i++){
			wordsMap.put(getRandomString(), random.nextInt(100));
		}
	}
	private void updateWordsMap() {
		for(Entry<String, Integer> entry : this.wordsMap.entrySet()) { //shouldn't be with iterator better?
		    Integer value = entry.getValue();
		    Integer newVal = value + random.nextInt(growth);
		    wordsMap.put(entry.getKey(), newVal);
		    System.out.println("Zmiana mapy!!!!!!! ");
		}
	}
	//simple random String generator
	private String getRandomString() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.stringSize; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
	}
	//getter
	public int getNumberOfWords() {
		return this.numberOfWords;		
	}

}
