package main.java;

import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import main.java.generator.SimpleGenerator;
import main.java.plotter.SimplePlotter;


public class PlotterMain {
	
	public static void main(String[] args){
		int numberOfWords = 40;
		SimplePlotter plotter = new SimplePlotter();
		SimpleGenerator simpleGenerator = new SimpleGenerator(numberOfWords);
		Map<String, Integer> results = simpleGenerator.getActualWordsMap();
		System.out.println(results);
		plotter.update(results);
		JFrame frame = new JFrame("Wykres");
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(plotter);
		frame.setSize(500, 400);
		frame.setVisible(true);
		for(int i=0; i< 10; ++i){
			try{
				Thread.sleep(1000);
				
			}catch(InterruptedException e){
				
			}
			results = simpleGenerator.getActualWordsMap();
			System.out.println(results);
			plotter.update(results);
		}
	}
}
